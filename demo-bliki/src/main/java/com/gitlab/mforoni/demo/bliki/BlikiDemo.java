package com.gitlab.mforoni.demo.bliki;

import java.io.IOException;
import java.util.List;
import info.bliki.api.Page;
import info.bliki.api.User;
import info.bliki.wiki.model.WikiModel;

/**
 * See this <a href=
 * "http://www.integratingstuff.com/2012/04/06/hook-into-wikipedia-using-java-and-the-mediawiki-api/">link</a>.
 * 
 * @author Marco Foroni
 */
final class BlikiDemo {

  private BlikiDemo() {}

  private void start(final String[] titles) throws IOException {
    final User user = new User("", "", "https://en.wikipedia.org/w/api.php");
    final boolean result = user.login();
    System.out.println("Logging result: " + result);
    final List<Page> pages = user.queryContent(titles);
    for (final Page page : pages) {
      final WikiModel wikiModel = new WikiModel("${image}", "${title}");
      final String html = wikiModel.render(page.toString());
      System.out.println(html);
    }
  }

  public static void main(final String[] args) throws IOException {
    final String[] titles = {"Web service", "National_Basketball_Association"};
    new BlikiDemo().start(titles);
  }
}
