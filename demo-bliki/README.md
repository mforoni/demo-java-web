# Demo bliki

This is a demo project to test *bliki core API* as explained in this [article](http://www.integratingstuff.com/2012/04/06/hook-into-wikipedia-using-java-and-the-mediawiki-api/).

## About [info.bliki](https://bitbucket.org/axelclk/info.bliki.wiki/wiki/Home)

The Java Wikipedia API (Bliki engine) is a parser library for converting Wikipedia wikitext notation to HTML. The project is hosted on [github](https://github.com/idio/bliki).
