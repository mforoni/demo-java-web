package com.gitlab.mforoni.demo.json2flat;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.github.opendevl.JFlat;

final class DemoJson2Flat {

  private static final Logger LOGGER = LoggerFactory.getLogger(DemoJson2Flat.class);
  private static final String OUTPUT_DIR = "output";
  private static final String ASSISTS_JSON = "fit/assists.json";
  private static final String ASSISTS_CSV = "assists.csv";
  private static final String FILE_JSON = "file.json";
  private static final String FILE_CSV = "file.csv";
  private static final String SKILLS_JSON = "skills.json";
  private static final String SKILLS_CSV = "skills.csv";
  private static final String HEROES_JSON = "heroes.json";
  private static final String HEROES_CSV = "heroes.csv";

  private DemoJson2Flat() {}

  private static void json2csv(final String jsonName, final String outputDir, final String csvName)
      throws IOException, URISyntaxException {
    final String str =
        new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource(jsonName).toURI())));
    final JFlat flatMe = new JFlat(str);
    // get the 2D representation of JSON document
    final List<Object[]> json2csv = flatMe.json2Sheet().getJsonAsSheet();
    for (final Object[] objects : json2csv) {
      LOGGER.info("{}", Arrays.asList(objects));
    }
    // write the 2D representation in csv format
    final Path dir = Files.createDirectories(Paths.get(outputDir));
    flatMe.write2csv(dir.getFileName() + File.separator + csvName);
    LOGGER.info("Generated file {} in the directory {} starting from file {}", csvName, outputDir,
        jsonName);
  }

  public static void main(final String[] args) {
    try {
      json2csv(FILE_JSON, OUTPUT_DIR, FILE_CSV);
      json2csv(ASSISTS_JSON, OUTPUT_DIR, ASSISTS_CSV);
      json2csv(SKILLS_JSON, OUTPUT_DIR, SKILLS_CSV);
      json2csv(HEROES_JSON, OUTPUT_DIR, HEROES_CSV);
    } catch (final IOException | URISyntaxException e) {
      e.printStackTrace();
    }
  }
}
