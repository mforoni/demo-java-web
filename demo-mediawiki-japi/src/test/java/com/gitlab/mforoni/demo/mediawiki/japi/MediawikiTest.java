package com.gitlab.mforoni.demo.mediawiki.japi;

import com.bitplan.mediawiki.japi.Mediawiki;
import junit.framework.TestCase;

public class MediawikiTest extends TestCase {

  /**
   * https://www.mediawiki.org/wiki/API:Query#Sample_query
   * https://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvprop=content&titles=Main%20Page&format=xml
   */
  public void testGetPageContentString() {
    try {
      final Mediawiki wiki = new Mediawiki("https://en.wikipedia.org");
      final String content = wiki.getPageContent("Main Page");
      assertTrue(content.contains("Wikipedia"));
    } catch (final Exception ex) {
      ex.printStackTrace();
      fail();
    }
  }
}
