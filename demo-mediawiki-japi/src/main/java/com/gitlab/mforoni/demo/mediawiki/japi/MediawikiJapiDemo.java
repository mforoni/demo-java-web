package com.gitlab.mforoni.demo.mediawiki.japi;

import com.bitplan.mediawiki.japi.Mediawiki;


final class MediawikiJapiDemo {
  // https://github.com/WolfgangFahl/Mediawiki-Japi
  private MediawikiJapiDemo() {}

  public static void sampleQuery(final String siteUrl, final String pageTitle) throws Exception {
    final Mediawiki wiki = new Mediawiki(siteUrl);
    final String content = wiki.getPageContent(pageTitle);
    System.out.println(content);
  }

  public static void main(final String[] args) throws Exception {
    sampleQuery("https://en.wikipedia.org", "National_Basketball_Association");
    // sampleQuery("https://feheroes.gamepedia.com", "Celica"); // not working
  }
}
