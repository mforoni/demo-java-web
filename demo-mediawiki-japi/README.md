# Demo Mediawiki-Japi

Demo project to test this Java library useful to call [Mediawiki API](https://www.mediawiki.org/wiki/API:Main_page).

## About [Mediawiki-Japi](http://mediawiki-japi.bitplan.com/index.php/Main_Page)

The Mediawiki-Japi is just another Mediawiki API for Java.

## About [Mediawiki](https://www.mediawiki.org/wiki/MediaWiki)

MediaWiki is a free and open source software wiki package written in PHP, originally for use on Wikipedia