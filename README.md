# Java Web APIs Demos

Contains demo examples of several Java APIs of famous websites or related to web aspects like json manipulation:

* demo-bliki
* demo-flickr4java
* demo-gson
* [demo-jetty](#demo-jetty)
* demo-json2flat
* demo-mediawiki-japi

## About [bliki](https://github.com/idio/bliki)

The Java Wikipedia API (Bliki engine) is a parser library for converting Wikipedia wikitext notation to HTML.

## About [Flickr4Java](https://github.com/boncey/Flickr4Java)

This is a Java API which wraps the [REST-based Flickr API](http://www.flickr.com/services/api/).

## About [Gson](https://github.com/google/gson)

*Gson* is a Java library that can be used to convert Java Objects into their JSON representation. It can also be used to convert a JSON string to an equivalent Java object. Gson can work with arbitrary Java objects including pre-existing objects that you do not have source-code of.

## demo-jetty

Eclipse Jetty provides a Web server and `javax.servlet` container, plus support for HTTP/2, WebSocket, OSGi, JMX, JNDI, JAAS and many other integrations.
###### Source: [eclipse.org/jetty/](https://www.eclipse.org/jetty/)

The demo project is a simple HelloWorld servlet created following this [guide](https://wiki.eclipse.org/Jetty/Tutorial/Jetty_and_Maven_HelloWorld).


## About [Json2Flat](https://github.com/opendevl/Json2Flat)

This library converts JSON documents to CSV.
It uses google-gson and JsonPath for conversion.

## About [Mediawiki-Japi](https://github.com/WolfgangFahl/Mediawiki-Japi)

Java library to call [Mediawiki API](http://www.mediawiki.org/wiki/API:Main_page).


## TODO

* test crawler4j