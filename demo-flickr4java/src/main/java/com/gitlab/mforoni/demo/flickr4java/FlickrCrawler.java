package com.gitlab.mforoni.demo.flickr4java;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.FlickrException;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.photos.Photo;
import com.flickr4java.flickr.photos.PhotoList;
import com.flickr4java.flickr.photos.SearchParameters;

/**
 * See this <a href=
 * "https://github.com/boncey/Flickr4Java/blob/master/Flickr4Java/src/examples/java/FlickrCrawler">link</a>.
 * <p>
 * Note:
 * <ul>
 * <li>Java 7 is needed</li>
 * <lI>insert your api</li>
 * <lI>and secretkey</li>
 * </ul>
 * Start main with wanted tags as parameter, for example:
 * 
 * <pre>
 * FlickrCrawler.main(Sunset)
 * </pre>
 * 
 * and all pics will be saved in original size or large to pics\sunset\...
 */
final class FlickrCrawler {

  private static String path = "";
  private static Preferences userPrefs = Preferences.userNodeForPackage(FlickrCrawler.class);

  // convert filename to clean filename
  public static String convertToFileSystemChar(final String name) {
    String erg = "";
    final Matcher m =
        Pattern.compile("[a-z0-9 _#&@\\[\\(\\)\\]\\-\\.]", Pattern.CASE_INSENSITIVE).matcher(name);
    while (m.find()) {
      erg += name.substring(m.start(), m.end());
    }
    if (erg.length() > 200) {
      erg = erg.substring(0, 200);
      System.out.println("cut filename: " + erg);
    }
    return erg;
  }

  public static boolean saveImage(final Flickr f, final Photo p) {
    final String cleanTitle = convertToFileSystemChar(p.getTitle());
    final File orgFile = new File(
        path + File.separator + cleanTitle + "_" + p.getId() + "_o." + p.getOriginalFormat());
    final File largeFile = new File(
        path + File.separator + cleanTitle + "_" + p.getId() + "_b." + p.getOriginalFormat());
    if (orgFile.exists() || largeFile.exists()) {
      System.out.println(p.getTitle() + "\t" + p.getLargeUrl() + " skipped!");
      return false;
    }
    try {
      final Photo nfo = f.getPhotosInterface().getInfo(p.getId(), null);
      if (nfo.getOriginalSecret().isEmpty()) {
        ImageIO.write(p.getLargeImage(), p.getOriginalFormat(), largeFile);
        System.out.println(
            p.getTitle() + "\t" + p.getLargeUrl() + " was written to " + largeFile.getName());
      } else {
        p.setOriginalSecret(nfo.getOriginalSecret());
        ImageIO.write(p.getOriginalImage(), p.getOriginalFormat(), orgFile);
        System.out.println(
            p.getTitle() + "\t" + p.getOriginalUrl() + " was written to " + orgFile.getName());
      }
    } catch (final FlickrException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return true;
  }

  public static void main(final String[] args) {
    if (args.length != 3) {
      System.out.println(
          "Parameter are needed as for searching. Example: FlickrCrawler.java <apikey> <secret> <sunset ...>");
      return;
    }
    final Flickr flickr = new Flickr(args[0], args[1], new REST());
    final SearchParameters searchParameters = new SearchParameters();
    searchParameters.setAccuracy(1);
    final StringBuilder tagsBuilder = new StringBuilder();
    for (int i = 2; i < args.length; i++) {
      tagsBuilder.append(" " + args[i]);
    }
    path = "pics" + File.separator + tagsBuilder.toString().substring(1);
    new File(path).mkdirs();
    searchParameters.setTags(args);
    for (int i = userPrefs.getInt(path, 0); true; i++) {
      userPrefs.putInt(path, i);
      System.out.println("\tcurrent page: " + userPrefs.getInt(path, 0));
      try {
        final PhotoList<Photo> list = flickr.getPhotosInterface().search(searchParameters, 500, i);
        if (list.isEmpty()) {
          break;
        }
        final Iterator<Photo> itr = list.iterator();
        while (itr.hasNext()) {
          saveImage(flickr, itr.next());
        }
      } catch (final FlickrException e) {
        e.printStackTrace();
      }
    }
  }
}
