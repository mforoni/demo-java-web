package com.gitlab.mforoni.demo.flickr4java;

import java.util.Collection;
import java.util.Collections;
import org.w3c.dom.Element;
import com.flickr4java.flickr.Flickr;
import com.flickr4java.flickr.REST;
import com.flickr4java.flickr.test.TestInterface;

final class VerifyFlickrConnection {

  private VerifyFlickrConnection() {}

  public static void main(final String[] args) {
    if (args.length != 2) {
      System.err.println("Please specify your flickr API key and shared secret from command line");
      System.exit(1);
    }
    try {
      final String apiKey = args[0];
      final String sharedSecret = args[1];

      final Flickr f = new Flickr(apiKey, sharedSecret, new REST());
      final TestInterface testInterface = f.getTestInterface();
      final Collection<Element> results =
          testInterface.echo(Collections.<String, String>emptyMap());
      System.out.println(results);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }
}
