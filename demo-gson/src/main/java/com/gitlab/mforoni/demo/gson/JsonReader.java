package com.gitlab.mforoni.demo.gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

final class JsonReader {

  private JsonReader() {
    throw new AssertionError();
  }

  private static void readJson(final String path) throws FileNotFoundException, IOException {
    final Gson gson = new Gson();
    try (final Reader reader = new FileReader(
        Thread.currentThread().getContextClassLoader().getResource(path).getFile())) {
      final JsonElement element = gson.fromJson(reader, JsonElement.class);
      if (element.isJsonNull()) {
        System.out.println(element.toString());
      } else if (element.isJsonArray()) {
        final JsonArray array = element.getAsJsonArray();
        System.out.println(array.toString());
      } else if (element.isJsonObject()) {
        final JsonObject jsonObject = element.getAsJsonObject();
        for (final Map.Entry<String, JsonElement> entry : jsonObject.entrySet()) {
          System.out.println("Key = " + entry.getKey());
          System.out.println("Value = " + entry.getValue());
        }
      } else if (element.isJsonPrimitive()) {
        System.out.println(element.toString());
      }
    }
  }

  public static void main(final String[] args) {
    try {
      readJson("assists.json");
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
