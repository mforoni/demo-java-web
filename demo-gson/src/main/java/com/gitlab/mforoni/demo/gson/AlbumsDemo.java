package com.gitlab.mforoni.demo.gson;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author Marco Foroni
 */
final class AlbumsDemo {
  // http://www.studytrails.com/java/json/java-google-json-parse-json-to-java/

  private static void start() {
    final Albums albums = new Albums();
    albums.title = "Free Music Archive - Albums";
    albums.message = "";
    albums.total = "11259";
    albums.total_pages = 2252;
    albums.page = 1;
    albums.limit = "5";
    final GsonBuilder builder = new GsonBuilder();
    builder.setPrettyPrinting().serializeNulls();
    builder.setFieldNamingStrategy(new FieldNamingStrategy() {

      @Override
      public String translateName(final Field f) {
        if (f.getName().equals("albumId")) {
          return "album_id";
        } else {
          return f.getName();
        }
      }
    });
    final Gson gson = builder.create();
    final Dataset dataset = new Dataset();
    dataset.album_id = "7596";
    dataset.album_title = "Album 1";
    final AlbumImages image = new AlbumImages();
    image.image_id = "1";
    image.albumId = "10";
    dataset.images.add(image);
    albums.dataset.add(dataset);
    System.out.println(gson.toJson(albums));
  }

  public static void main(final String[] args) {
    start();
  }

  static class Albums {

    String title;
    String message;
    List<String> errors = new ArrayList<>();
    String total;
    int total_pages;
    int page;
    String limit;
    List<Dataset> dataset = new ArrayList<>();
  }

  static class Dataset {

    String album_id;
    String album_title;
    @SerializedName("album_images")
    List<AlbumImages> images = new ArrayList<>();
  }

  static class AlbumImages {

    String image_id;
    String user_id;
    String albumId;
  }
}
