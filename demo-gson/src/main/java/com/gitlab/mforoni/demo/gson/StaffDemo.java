package com.gitlab.mforoni.demo.gson;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

/**
 * 
 * @author Marco Foroni
 */
final class StaffDemo {
  // https://www.mkyong.com/java/how-do-convert-java-object-to-from-json-format-gson-api/

  public static void main(final String[] args) {
    toJson();
    fromJson();
    fromJsonToString();
  }

  private static void toJson() {
    final File file = new File("output");
    file.mkdirs();
    final Staff staff = createDummyObject();
    // 1. Convert object to JSON string
    final Gson gson = new Gson();
    final String json = gson.toJson(staff);
    System.out.println(json);
    // 2. Convert object to JSON string and save into a file directly
    try (FileWriter writer = new FileWriter("output/staff.json")) {
      gson.toJson(staff, writer);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private static void fromJson() {
    final Gson gson = new Gson();
    try (Reader reader = new FileReader("output/staff.json")) {
      // Convert JSON to Java Object
      final Staff staff = gson.fromJson(reader, Staff.class);
      System.out.println(staff);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private static void fromJsonToString() {
    final Gson gson = new Gson();
    try (Reader reader = new FileReader("output/staff.json")) {
      // Convert JSON to JsonElement, and later to String
      final JsonElement json = gson.fromJson(reader, JsonElement.class);
      final String jsonInString = gson.toJson(json);
      System.out.println(jsonInString);
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  private static Staff createDummyObject() {
    final Staff staff = new Staff();
    staff.setName("mkyong");
    staff.setAge(35);
    staff.setPosition("Founder");
    staff.setSalary(new BigDecimal("10000"));
    final List<String> skills = new ArrayList<>();
    skills.add("java");
    skills.add("python");
    skills.add("shell");
    staff.setSkills(skills);
    return staff;
  }

  public static class Staff {

    private String name;
    private int age;
    private String position;
    private BigDecimal salary;
    private List<String> skills;

    public String getName() {
      return name;
    }

    public void setName(final String name) {
      this.name = name;
    }

    public int getAge() {
      return age;
    }

    public void setAge(final int age) {
      this.age = age;
    }

    public String getPosition() {
      return position;
    }

    public void setPosition(final String position) {
      this.position = position;
    }

    public BigDecimal getSalary() {
      return salary;
    }

    public void setSalary(final BigDecimal salary) {
      this.salary = salary;
    }

    public List<String> getSkills() {
      return skills;
    }

    public void setSkills(final List<String> skills) {
      this.skills = skills;
    }

    @Override
    public String toString() {
      return name + " " + age;
    }
  }
}
